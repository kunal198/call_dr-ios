//
//  SearchDoctor_ViewController.swift
//  CallDr
//
//  Created by Brst on 7/14/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import MessageUI

protocol HandleMapSearch: class {
    func dropPinZoomIn(_ placemark:MKPlacemark)
}
@available(iOS 9.0, *)

class SearchDoctor_ViewController: UIViewController , UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, MFMailComposeViewControllerDelegate{

    
    @IBOutlet var txt_location: UILabel!
    @IBOutlet var txt_search: UITextField!
    @IBOutlet var btn_ratings: UIButton!
    @IBOutlet var txt_date: UITextField!
    @IBOutlet var view_fees: UIView!
    @IBOutlet var btn_fees: UIButton!
    @IBOutlet var view_rating: UIView!
    @IBOutlet var tableView_rating: UITableView!
    @IBOutlet var tableView_fees: UITableView!
    @IBOutlet var tableView_profession: UITableView!
    @IBOutlet var view1: UIView!
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var container: UIView!
    @IBOutlet var view_search: UIView!
    @IBOutlet var view11: UIView!
    @IBOutlet var subView: UIView!
    
    
    
    var array_fees = NSMutableArray()
    var datePicker : UIDatePicker!
    var array_rating = NSMutableArray()
    
    var selectedPin: MKPlacemark? //
    var latitude_party = Double()
    var longitude_party = Double()
    var location_check = Bool()
    var strLocation = String()
    var resultSearchController: UISearchController!
    let locationManager = CLLocationManager()
    var array_doctor_profession = NSMutableArray()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        array_fees = ["LOW TO HIGH", "HIGH TO LOW"]
        array_rating = ["1 STAR", "2 STAR", "3 STAR", "4 STAR", "5 STAR"]
        
      array_doctor_profession = ["SEARCH PROFESSION","Allergist", "Andrologist", "Anaesthesiologist", "Cardiologist", "Cardiac Electrophysiologist", "Dermatologist", "Endocrinologist", "Epidemiologist", "Gastroenterologist", "Geriatrician", "Hyperbaric Physician", "Hematologist", "Hepatologist", "Immunologist", "Oral Surgeon", "Neonatologist", "Neurologist", "Gynecologist", "Oncologist", "Otolaryngologist", "Parasitologist", "Plastic Surgeon", "Pulmonologist", "Urologist", "Veterinarian"]
        
        
        
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            self.view11.backgroundColor = UIColor.white
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.view11.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            self.view11.addSubview(blurEffectView)
        }
        
        self.subView.layer.shadowColor = UIColor.lightGray.cgColor
        self.subView.layer.shadowOpacity = 0.5
        self.subView.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.subView.layer.shadowRadius = 10
        self.subView.layer.cornerRadius = 5
        self.subView.layer.masksToBounds = false
        self.subView.layer.shadowPath = UIBezierPath(rect: self.subView.bounds).cgPath
        self.subView.layer.shouldRasterize = true
        self.subView.layer.rasterizationScale =  UIScreen.main.scale
        
        // Gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapAction(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        
        view11.addGestureRecognizer(tapGesture)

        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_search_selected(_ sender: Any)
    {
        self.tableView_profession.reloadData()
        self.view_search.isHidden = false
    }
    @IBAction func btn_rating_selected(_ sender: Any)
    {
        self.tableView_rating.reloadData()
        self.view_rating.isHidden = false
    }
    //MARK: Fees
    @IBAction func btn_fees_selected(_ sender: Any)
    {
        self.tableView_fees.reloadData()
        self.view_fees.isHidden = false
    }
    @IBAction func btn_search_doctor(_ sender: Any)
    {
        UserDefaults.standard.set("patient", forKey: "user")
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "search_doc") as! SelectDoctor_ViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)

    }
    
    
//MARK: Table View Functions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableView_rating
        {
            return self.array_rating.count
        }
        if tableView == tableView_fees
        {
            return self.array_fees.count
        }
        if tableView == tableView_profession
        {
            return self.array_doctor_profession.count
        }
       return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyBooking_TableViewCell
        if tableView == tableView_rating
        {
            cell.lbl1.text = self.array_rating[indexPath.row] as? String
            return cell
        }
        if tableView == tableView_fees
        {
            cell.lbl1.text = self.array_fees[indexPath.row] as? String
            return cell
        }
        if tableView == tableView_profession
        {
            cell.lbl1.text = self.array_doctor_profession[indexPath.row] as? String
            return cell
        }
          return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableView_rating
        {
            let data = ("RATINGS (\(self.array_rating[indexPath.row]     as! String))")
            self.btn_ratings.setTitle(data, for: UIControlState.normal)
            self.view_rating.isHidden = true
            
        }
        if tableView == tableView_fees
        {
            let data = ("FEES (\(self.array_fees[indexPath.row]     as! String))")
            self.btn_fees.setTitle(data, for: UIControlState.normal)
            self.view_fees.isHidden = true
        }
        if tableView == tableView_profession
        {
            self.txt_search.text = self.array_doctor_profession[indexPath.row] as? String
            self.view_search.isHidden = true
        }
    }
    
//MARK: text field functions
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickUpDate(self.txt_date)
    }
    private func textFieldDidEndEditing(_ textField: UITextField)-> Bool {
        
        return true;
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.view.frame.origin.y = 0;
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }

    //MARK:- Function of datePicker
    func pickUpDate(_ textField : UITextField){
        //MARK: ^^^ DatePicker
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 150))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePickerMode.dateAndTime
        self.datePicker.layer.masksToBounds = true
        textField.inputView = self.datePicker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        // toolBar.backgroundColor = UIColor.red
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(SearchDoctor_ViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(SearchDoctor_ViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    // Button Done and Cancel
    func doneClick() {
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .long
        dateFormatter1.timeStyle = .short
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, MMMM/dd/yyyy hh:mm a Z"
        txt_date.text = dateFormatter1.string(from: datePicker.date)
        txt_date.resignFirstResponder()
    }
    func cancelClick() {
        txt_date.resignFirstResponder()
    }
//MARK: Location search :
    @IBAction func btn_map_selected(_ sender: Any)
    {
      self.tabBarController?.tabBar.isHidden = true;
        
        view1.isHidden = false
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        let locationSearchTable = storyboard!.instantiateViewController(withIdentifier: "LocationSearchTable") as! LocationSearch_TableViewController
        resultSearchController = UISearchController(searchResultsController: locationSearchTable)
        resultSearchController.searchResultsUpdater = locationSearchTable
        let searchBar = resultSearchController!.searchBar
        searchBar.sizeToFit()
        self.mapView .addSubview(searchBar)
        
        searchBar.placeholder = "Search for places"
        container = resultSearchController?.searchBar
        resultSearchController.hidesNavigationBarDuringPresentation = false
        resultSearchController.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
        locationSearchTable.mapView = mapView
        locationSearchTable.handleMapSearchDelegate = self
       
    }
   
    @IBAction func btn_menu(_ sender: Any)
    {
        self.view11.isHidden = false
        self.subView.isHidden = false

    }
    @IBAction func btn_location_save(_ sender: Any)
    {
        self.view1.isHidden = true
        self.tabBarController?.tabBar.isHidden = false
    }

    @IBAction func btn_location_cancel(_ sender: Any)
    {
        self.view1.isHidden = true
        self.tabBarController?.tabBar.isHidden = false

    }
//MARK: History
    @IBAction func btn_history_selected(_ sender: Any)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "history") as! History_ViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)

    }
   
//MARK: Contact
    @IBAction func btn_contact_selected(_ sender: Any)
    {
        if MFMailComposeViewController.canSendMail()
        {
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            // Configure the fields of the interface.
            composeVC.setToRecipients(["address@example.com"])
            composeVC.setSubject("Hello!")
            composeVC.setMessageBody("Hello this is my message body!", isHTML: false)
            // Present the view controller modally.
            self.present(composeVC, animated: true, completion: nil)
            return
        }        else {
            // Let the user know if his/her device isn't able to send text messages
            let errorAlert = UIAlertView(title: "Cannot Send Mail", message: "Your device is not able to send Mail.", delegate: self, cancelButtonTitle: "OK")
            errorAlert.show()
        }

    }
//MARK: Logout
    @IBAction func btn_logout(_ sender: Any)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "sign_in") as! ViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)

    }
    
    
    //MARK: <<< Gesture function : Tap Action
    func tapAction(sender:UITapGestureRecognizer)
    {
        self.view11.isHidden = true
        self.subView.isHidden = true
    }
    
    //MARK: Deafult mail
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }

    
    
    
}



@available(iOS 9.0, *)
extension SearchDoctor_ViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if location_check == false
        {
            guard let location = locations.first else { return }
            let span = MKCoordinateSpanMake(0.05, 0.05)
            let region = MKCoordinateRegion(center: location.coordinate, span: span)
            mapView.setRegion(region, animated: true)
            CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
              //  print(location)
                
                if error != nil {
                    print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                    return
                }
                
                if (placemarks?.count)! > 0
                {
                    let pm = placemarks?[0]
                    // Location name
                    let locationName = pm?.addressDictionary!["SubLocality"] as? NSString
                    
                    // Street address
                    let city = pm?.addressDictionary!["City"] as? NSString
                    
                    // City
                    let state = pm?.addressDictionary!["State"] as? NSString
                    self.latitude_party = location.coordinate.latitude
                    self.longitude_party = location.coordinate.longitude
                    self.txt_location.text = "\(String(describing: locationName!))\(String(describing: city!))\(String(describing: state!))"
                 //   self.indicator_location.stopAnimating()
                 //   self.indicator_location.isHidden = true
                    
               //     self.btn_inviteAllFans.isUserInteractionEnabled = true
                }
                else {
                    print("Problem with the data received from geocoder")
                }
            })
            locationManager.stopUpdatingLocation()
        }
        else
        {
       //     self.indicator_location.stopAnimating()
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: \(error)")
    }
}

@available(iOS 9.0, *)
extension SearchDoctor_ViewController: HandleMapSearch {
    
    func dropPinZoomIn(_ placemark: MKPlacemark){
        // cache the pin
        selectedPin = placemark
        location_check = true
        // clear existing pins
        mapView.removeAnnotations(mapView.annotations)
        let annotation = MKPointAnnotation()
        annotation.coordinate = placemark.coordinate
        annotation.title = placemark.name
        if let city = placemark.locality,
            let state = placemark.administrativeArea
        {
            annotation.subtitle = "\(city) \(state)"
        }
        latitude_party = placemark.coordinate.latitude
        longitude_party = placemark.coordinate.longitude
        
        let placename = placemark.name
        let placelocality = placemark.locality
        var finalplace = ""
        if(placename != nil)
        {
            finalplace = placename!
        } else
        {
            finalplace = ""
        }
        if(placelocality != nil)
        {
            if(finalplace != "")
            {
                finalplace = "\(finalplace)\(",")\(placelocality!)"
            } else {
                finalplace = "\(String(describing: placelocality))"
            }
        } else {
        }
        
        self.txt_location.text = "\(finalplace)"
   //     self.indicator_location.stopAnimating()
    //    self.indicator_location.isHidden = true
    //    self.btn_inviteAllFans.isUserInteractionEnabled = true
        
        mapView.addAnnotation(annotation)
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegionMake(placemark.coordinate, span)
        mapView.setRegion(region, animated: true)
    }
    
}

@available(iOS 9.0, *)
extension SearchDoctor_ViewController : MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?{
        
        guard !(annotation is MKUserLocation) else { return nil }
        let reuseId = "pin"
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
        if pinView == nil {
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
        }
        pinView?.pinTintColor = UIColor.red
        pinView?.canShowCallout = true
        let smallSquare = CGSize(width: 30, height: 30)
        let button = UIButton(frame: CGRect(origin: CGPoint.zero, size: smallSquare))
        button.setBackgroundImage(UIImage(named: "location-1"), for: UIControlState())
        // button.addTarget(self, action: #selector(Create_WatchParty_ViewController.getDirections), for: .touchUpInside)
        pinView?.leftCalloutAccessoryView = button
        
        return pinView
}
}
