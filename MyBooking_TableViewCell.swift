//
//  MyBooking_TableViewCell.swift
//  CallDr
//
//  Created by Brst on 7/14/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit

class MyBooking_TableViewCell: UITableViewCell {

    @IBOutlet var lbl1: UILabel!
    @IBOutlet var txt_calender: UITextField!
 
    @IBOutlet var img_check: UIImageView!
    @IBOutlet var btn_1: UIButton!
    @IBOutlet var btn_3: UIButton!
    @IBOutlet var btn_2: UIButton!
    @IBOutlet var img: UIImageView!
    @IBOutlet var lbl_name: UILabel!
    @IBOutlet var lbl_address: UILabel!
    @IBOutlet var lbl_calender: UILabel!
    @IBOutlet var btn_cancel: UIButton!
    @IBOutlet var btn_accepted: UIButton!
    
    @IBOutlet var view_date: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
