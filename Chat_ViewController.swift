//
//  Chat_ViewController.swift
//  CallDr
//
//  Created by Brst on 7/18/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit

class Chat_ViewController: UIViewController, UITextViewDelegate {

    @IBOutlet var view_chat_send: UIView!
    @IBOutlet var txt_addText: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        txt_addText.delegate = self
        self.view_chat_send.layer.cornerRadius = self.view_chat_send.frame.size.width/2
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }

    
   
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.txt_addText.text = ""
    }
  
}
