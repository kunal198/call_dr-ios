//
//  Profile_ViewController_doc.swift
//  CallDr
//
//  Created by Brst on 7/17/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import MessageUI

class Profile_ViewController_doc: UIViewController, UIGestureRecognizerDelegate, UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate {

    @IBOutlet var txt_doc_profession: UITextField!
    @IBOutlet var view_doc_profession: UIView!
    @IBOutlet var scr: UIScrollView!
    @IBOutlet var view_details: UIView!
    @IBOutlet var view11: UIView!
    @IBOutlet var subView: UIView!
    @IBOutlet var view_changePassword: UIView!
    
    @IBOutlet var txt_firstName: UITextField!
    @IBOutlet var txt_lastName: UITextField!
    @IBOutlet var btn_profession: UIButton!
    @IBOutlet var txt_fees: UITextField!
    @IBOutlet var txt_location: UITextField!
    @IBOutlet var txt_zip: UITextField!
    @IBOutlet var txt_email: UITextField!
    @IBOutlet var txt_phone: UITextField!
    @IBOutlet var btn_edit: UIButton!
    @IBOutlet var btn_changePassword: UIButton!
    
    var array_doctor_profession = NSMutableArray()
    var checkEdit = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.scr.contentSize = CGSize(width: self.view_details.frame.size.width, height: self.view_details.frame.size.height + 200)
        
          array_doctor_profession = ["Allergist", "Andrologist", "Anaesthesiologist", "Cardiologist", "Cardiac Electrophysiologist", "Dermatologist", "Endocrinologist", "Epidemiologist", "Gastroenterologist", "Geriatrician", "Hyperbaric Physician", "Hematologist", "Hepatologist", "Immunologist", "Oral Surgeon", "Neonatologist", "Neurologist", "Gynecologist", "Oncologist", "Otolaryngologist", "Parasitologist", "Plastic Surgeon", "Pulmonologist", "Urologist", "Veterinarian"]
        
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            self.view11.backgroundColor = UIColor.white
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.view11.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            self.view11.addSubview(blurEffectView)
        }
        
        self.subView.layer.shadowColor = UIColor.lightGray.cgColor
        self.subView.layer.shadowOpacity = 0.5
        self.subView.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.subView.layer.shadowRadius = 10
        self.subView.layer.cornerRadius = 5
        self.subView.layer.masksToBounds = false
        self.subView.layer.shadowPath = UIBezierPath(rect: self.subView.bounds).cgPath
        self.subView.layer.shouldRasterize = true
        self.subView.layer.rasterizationScale =  UIScreen.main.scale
        
        self.view_changePassword.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_changePassword.layer.shadowOpacity = 0.5
        self.view_changePassword.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_changePassword.layer.shadowRadius = 10
        self.view_changePassword.layer.cornerRadius = 20
        self.view_changePassword.layer.masksToBounds = false
        self.view_changePassword.layer.shadowPath = UIBezierPath(rect: self.view_changePassword.bounds).cgPath
        self.view_changePassword.layer.shouldRasterize = true
        self.view_changePassword.layer.rasterizationScale =  UIScreen.main.scale
        
        // Gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapAction(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        
        view11.addGestureRecognizer(tapGesture)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.checkEdit = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_edit_details(_ sender: Any)
    {
        if checkEdit == false
        {
            checkEdit = true
            self.btn_edit.setTitle("SUBMIT", for: UIControlState.normal)
            
            self.txt_firstName.isUserInteractionEnabled = true
            self.txt_lastName.isUserInteractionEnabled = true
            self.btn_profession.isUserInteractionEnabled = true
            self.txt_fees.isUserInteractionEnabled = true
            self.txt_location.isUserInteractionEnabled = true
            self.txt_zip.isUserInteractionEnabled = true
            self.txt_email.isUserInteractionEnabled = true
            self.txt_phone.isUserInteractionEnabled = true
        }
        else
        {
            self.checkEdit = false
            self.btn_edit.setTitle("EDIT", for: UIControlState.normal)
            
            self.txt_firstName.isUserInteractionEnabled = false
            self.txt_lastName.isUserInteractionEnabled = false
            self.btn_profession.isUserInteractionEnabled = false
            self.txt_fees.isUserInteractionEnabled = false
            self.txt_location.isUserInteractionEnabled = false
            self.txt_zip.isUserInteractionEnabled = false
            self.txt_email.isUserInteractionEnabled = false
            self.txt_phone.isUserInteractionEnabled = false
        }
    }
    
    @IBAction func btn_change_password(_ sender: Any)
    {
        self.view11.isHidden = false
        self.view_changePassword.isHidden = false
    }
    @IBAction func btn_doc_profession(_ sender: Any)
    {
        self.view_doc_profession.isHidden = false
    }
    @IBAction func btn_menu(_ sender: Any)
    {
        self.view11.isHidden = false
        self.subView.isHidden = false
    }
    @IBAction func btn_history_selected(_ sender: Any)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "history_doc") as! History_Doc_ViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)

    }
    @IBAction func btn_contact(_ sender: Any)
    {
        if MFMailComposeViewController.canSendMail()
        {
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            // Configure the fields of the interface.
            composeVC.setToRecipients(["address@example.com"])
            composeVC.setSubject("Hello!")
            composeVC.setMessageBody("Hello this is my message body!", isHTML: false)
            // Present the view controller modally.
            self.present(composeVC, animated: true, completion: nil)
            return
        }        else {
            // Let the user know if his/her device isn't able to send text messages
            let errorAlert = UIAlertView(title: "Cannot Send Mail", message: "Your device is not able to send Mail.", delegate: self, cancelButtonTitle: "OK")
            errorAlert.show()
        }

    }
    @IBAction func btn_logout_selected(_ sender: Any)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "sign_in") as! ViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)

    }

    //MARK: <<< Gesture function : Tap Action
    func tapAction(sender:UITapGestureRecognizer)
    {
        self.view11.isHidden = true
        self.subView.isHidden = true
        self.view_changePassword.isHidden = true
        
    }

    //Table View Functions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.array_doctor_profession.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyBooking_TableViewCell
        cell.lbl1.text = self.array_doctor_profession[indexPath.row] as? String
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.txt_doc_profession.text = self.array_doctor_profession[indexPath.row] as? String
        self.view_doc_profession.isHidden = true
    }
    //MARK: Deafult mail
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }

}
