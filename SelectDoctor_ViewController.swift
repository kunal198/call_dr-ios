//
//  SelectDoctor_ViewController.swift
//  CallDr
//
//  Created by Brst on 7/20/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit

class SelectDoctor_ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource , UIGestureRecognizerDelegate{

    
    @IBOutlet var view11: UIView!
    @IBOutlet var view_calender: UIView!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var btn_calender_cancel: UIButton!
    @IBOutlet var btn_calender_done: UIButton!
    @IBOutlet var tableView: UITableView!
    
    var datecheck = Bool()
    var calender = String()
    var indexCheck = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.calender = ""
        
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            self.view11.backgroundColor = UIColor.white
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.view11.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            self.view11.addSubview(blurEffectView)
        }
    
        // Gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapAction(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        
        view11.addGestureRecognizer(tapGesture)

        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_calender_done(_ sender: Any)
    {
        self.tabBarController?.tabBar.isHidden = false
        self.view11.isHidden = true
        self.view_calender.isHidden = true
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .long
        dateFormatter1.timeStyle = .short
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, MMMM/dd/yyyy hh:mm a Z"
        self.calender = dateFormatter1.string(from: datePicker.date)
        self.datecheck = true
        self.tableView.reloadData()
        
    }
    @IBAction func btn_calender_cancel(_ sender: Any)
    {
        self.tabBarController?.tabBar.isHidden = false
        self.calender = ""
        self.view11.isHidden = true
        self.view_calender.isHidden = true
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? MyBooking_TableViewCell
        if self.datecheck == true && self.calender != "" &&  self.indexCheck == indexPath.row
        {
            cell?.view_date.isHidden = false
            cell?.lbl_calender.text = self.calender
            cell?.btn_1.setTitle("REQUESTED", for: UIControlState.normal)
        }
        
        cell?.btn_1 .addTarget(self, action:#selector(self.buttonClicked1), for: .touchUpInside)
        cell?.btn_1.tag = indexPath.row
        
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        UserDefaults.standard.set("patient", forKey: "user")
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "book_doctor_profile") as! Book_Doc_profileViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.datecheck == true && indexPath.row == indexCheck
        {
            return 115
        }
        
        return 93
    }

    func buttonClicked1(sender: UIButton)
    {
        self.indexCheck = sender.tag
        self.tabBarController?.tabBar.isHidden = true
        
        self.view11.isHidden = false
        self.view_calender.isHidden = false
    }

    
    //MARK: <<< Gesture function : Tap Action
    func tapAction(sender:UITapGestureRecognizer)
    {
        self.tabBarController?.tabBar.isHidden = false
        self.view11.isHidden = true
        self.view_calender.isHidden = true
    }


    
}
