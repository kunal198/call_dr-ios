//
//  Message_ViewController.swift
//  CallDr
//
//  Created by Brst on 7/14/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import MessageUI

class Message_ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, MFMailComposeViewControllerDelegate {

    @IBOutlet var tableView_msg: UITableView!
    @IBOutlet var view11: UIView!
    @IBOutlet var subView: UIView!
    
    var array_name = NSMutableArray()
    var array_img = NSMutableArray()
    var array_msg = NSMutableArray ()
    var array_time = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        array_name = ["DR.Barry devito","Dr.Cassy Farrier","Dr.Cecelia GoldStein","Dr.Dominic Craft","Dr. Ray Lefever","Dr.Miki Winget"]
        array_msg = ["Hii...We have a appointment tomorrow","I am busy today","Hii...We have a appointment tomorrow","Hii...We have a appointment tomorrow","Hii...We have a appointment tomorrow","Hii...We have a appointment tomorrow"]
        array_img = ["1","2","3","4","5","6"]
        
        
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            self.view11.backgroundColor = UIColor.white
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.view11.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            self.view11.addSubview(blurEffectView)
        }
        
        self.subView.layer.shadowColor = UIColor.lightGray.cgColor
        self.subView.layer.shadowOpacity = 0.5
        self.subView.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.subView.layer.shadowRadius = 10
        self.subView.layer.cornerRadius = 5
        self.subView.layer.masksToBounds = false
        self.subView.layer.shadowPath = UIBezierPath(rect: self.subView.bounds).cgPath
        self.subView.layer.shouldRasterize = true
        self.subView.layer.rasterizationScale =  UIScreen.main.scale
        
        // Gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapAction(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        
        view11.addGestureRecognizer(tapGesture)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_menu(_ sender: Any)
    {
         self.view11.isHidden = false
        self.subView.isHidden = false

    }
    @IBAction func btn_history_selected(_ sender: Any)
    {
        let data = UserDefaults.standard.object(forKey: "user") as! String
        if data == "patient"
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "history") as! History_ViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        else
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "history_doc") as! History_Doc_ViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)

        }
    
    }
   
    @IBAction func btn_contact_selected(_ sender: Any)
    {
        if MFMailComposeViewController.canSendMail()
        {
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            // Configure the fields of the interface.
            composeVC.setToRecipients(["address@example.com"])
            composeVC.setSubject("Hello!")
            composeVC.setMessageBody("Hello this is my message body!", isHTML: false)
            // Present the view controller modally.
            self.present(composeVC, animated: true, completion: nil)
            return
        }        else {
            // Let the user know if his/her device isn't able to send text messages
            let errorAlert = UIAlertView(title: "Cannot Send Mail", message: "Your device is not able to send Mail.", delegate: self, cancelButtonTitle: "OK")
            errorAlert.show()
        }

    }
    
    @IBAction func btn_logout(_ sender: Any)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "sign_in") as! ViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    //MARK: <<< Gesture function : Tap Action
    func tapAction(sender:UITapGestureRecognizer)
    {
        self.view11.isHidden = true
        self.subView.isHidden = true
    }

    
//MARK: Table View Functions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableView_msg
        {
            return self.array_name.count
        }
        else
        {
            return 2
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Message_TableViewCell
        if tableView == tableView_msg
        {
            cell.lbl_name.text = self.array_name[indexPath.row] as?String
            cell.lbl_msg.text = self.array_msg[indexPath.row] as? String
            cell.img.image = UIImage(named: self.array_img[indexPath.row] as! String )
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "chat") as! Chat_ViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    //MARK: Deafult mail
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }

    
}
