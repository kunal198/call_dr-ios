//
//  ViewController.swift
//  CallDr
//
//  Created by Brst on 7/14/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var view1: UIView!
    @IBOutlet var view2: UIView!
    @IBOutlet var txt_userName: UITextField!
    @IBOutlet var txt_password: UITextField!
    @IBOutlet var view_sign_up: UIView!
    @IBOutlet var view_forgetPassword: UIView!
    @IBOutlet var view11: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txt_userName.attributedPlaceholder = NSAttributedString(string: "USERNAME",
                                                                     attributes: [NSForegroundColorAttributeName: UIColor.init(colorLiteralRed: 191, green: 248, blue: 172, alpha: 0.7)])
        self.txt_password.attributedPlaceholder = NSAttributedString(string: "PASSWORD",
                                                                     attributes: [NSForegroundColorAttributeName: UIColor.init(colorLiteralRed: 191, green: 248, blue: 172, alpha: 0.7)])
        
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            self.view_sign_up.backgroundColor = UIColor.white
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.view_sign_up.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            self.view_sign_up.addSubview(blurEffectView)
        }

        self.view_forgetPassword.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_forgetPassword.layer.shadowOpacity = 0.5
        self.view_forgetPassword.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_forgetPassword.layer.shadowRadius = 10
        self.view_forgetPassword.layer.cornerRadius = 20
        self.view_forgetPassword.layer.masksToBounds = false
        self.view_forgetPassword.layer.shadowPath = UIBezierPath(rect: self.view_forgetPassword.bounds).cgPath
        self.view_forgetPassword.layer.shouldRasterize = true
        self.view_forgetPassword.layer.rasterizationScale =  UIScreen.main.scale
        
        // Gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapAction(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        
        view11.addGestureRecognizer(tapGesture)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//MARK: Forget Password
    @IBAction func btn_forget_password(_ sender: Any)
    {
        self.view11.isHidden = false
        self.view_forgetPassword.isHidden = false
    }
//MARK:<<< Sign In
    @IBAction func btn_sign_in(_ sender: Any)
    {
        UserDefaults.standard.set("patient", forKey: "user")
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "tabBar") as! UITabBarController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
//MARK:<<< Sign UP
    @IBAction func btn_sign_up(_ sender: Any)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "sign_up") as! SignUP__ViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @IBAction func btn_forget_submit(_ sender: Any)
    {
        self.view_forgetPassword.isHidden = true
        self.view11.isHidden = true
    }
    
    //MARK: <<< Gesture function : Tap Action
    func tapAction(sender:UITapGestureRecognizer)
    {
        self.view11.isHidden = true
        self.view_forgetPassword.isHidden = true
    }
}

