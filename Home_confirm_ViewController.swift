//
//  Home_confirm_ViewController.swift
//  CallDr
//
//  Created by Brst on 7/19/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class Home_confirm_ViewController: UIViewController, CLLocationManagerDelegate, UIGestureRecognizerDelegate {

    @IBOutlet var view11: UIView!
    @IBOutlet var VIEW_FEES: UIView!
    @IBOutlet var mapView: MKMapView!
    var locationManager:CLLocationManager!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.determineMyCurrentLocation()
        
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            self.view11.backgroundColor = UIColor.white
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.view11.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            self.view11.addSubview(blurEffectView)
        }
        
        self.VIEW_FEES.layer.shadowColor = UIColor.lightGray.cgColor
        self.VIEW_FEES.layer.shadowOpacity = 0.5
        self.VIEW_FEES.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.VIEW_FEES.layer.shadowRadius = 10
        self.VIEW_FEES.layer.cornerRadius = 20
        self.VIEW_FEES.layer.masksToBounds = false
        self.VIEW_FEES.layer.shadowPath = UIBezierPath(rect: self.VIEW_FEES.bounds).cgPath
        self.VIEW_FEES.layer.shouldRasterize = true
        self.VIEW_FEES.layer.rasterizationScale =  UIScreen.main.scale
        
        // Gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapAction(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        
        view11.addGestureRecognizer(tapGesture)
        

    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_confirm_selected(_ sender: Any)
    {
        self.view11.isHidden = false
        self.VIEW_FEES.isHidden = false
    }
    @IBAction func btn_addFees_submit(_ sender: Any)
    {
        self.view11.isHidden = true
        self.VIEW_FEES.isHidden = true
    }
    @IBAction func btn_back(_ sender: Any)
    {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true);
        //self.navigationController?.popViewController(animated: true)
    }
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            //locationManager.startUpdatingHeading()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        
        // manager.stopUpdatingLocation()
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }

    //MARK: <<< Gesture function : Tap Action
    func tapAction(sender:UITapGestureRecognizer)
    {
        self.view11.isHidden = true
        self.VIEW_FEES.isHidden = true
    }

    
}
