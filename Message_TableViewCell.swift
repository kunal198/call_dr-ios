//
//  Message_TableViewCell.swift
//  CallDr
//
//  Created by Brst on 7/15/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit

class Message_TableViewCell: UITableViewCell {

    
    @IBOutlet var img: UIImageView!
    @IBOutlet var lbl_name: UILabel!
    @IBOutlet var lbl_time: UILabel!
    @IBOutlet var lbl_msg: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
